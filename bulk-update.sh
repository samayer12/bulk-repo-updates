#!/usr/bin/env bash

# TODO: Input validation/processing? Not worth it this early
while read -r target ;
do
  echo "Beep boop! I updated $target branch. Later on I'll make a Merge Request."
done < "$1"
