#!/usr/bin/env bash

# TODO: Input validation/processing? Not worth it this early
while read -r target;
do
  git checkout -b "$target"
done < "$1"

echo "INFO - Branches created"
git branch -a
