# Bulk Repo Updater-tagger

Attempt to demonstrate how changes on one project can be applied to multiple other projects to reduce developer toil.

## Why do I care?

* Making the same set of changes over and over again introduces risk for transcription errors.
* Manual updates to N projects does not scale for one developer team when N is large.
* It reduces developer toil so your devs spend more time doing meaningful work instead of manually creating releases.
* You spend more time working on what matters and can mentor junior devs on project specifics instead of waiting for other teams.

## Technical Details

This project is also configured to use `semantic-release` for nightly builds, trunk-based workflows, and merge-request based workflows.
Reference [auto-tagger](https://gitlab.com/samayer12/auto-tagger) for more details on that use case.

Simulate a bulk update by running a CI pipeline for the most-recent tagged commit.
This will trigger the `bulk-update` job in `.gitlab-ci.yml`.

Run a pipeline with the `CREATE_COMMIT` variable set to `1` to simulate development.
This creates a new commit to the project on `main` that meets the `patch` version criteria.
Finally, gitlab starts a `semantic-release` job and increments the `patch` version.

Be sure to set `GL_TOKEN` when testing.
Grant the token `api` access.
Expose `GL_TOKEN` as a variable in CI.

## Scheduled Jobs

* `Nightly build`: Runs `main` and tags if `semantic-release` requirements are met.

## Simulated Development Activity

* `create-commit.sh` will create a commit in the repo. This runs in CI using a project access token. `$CREATE_COMMIT` must be set.
* Programatically create a MR with the command:

```bash
git push --push-option=merge_request.create --push-option=merge_request.target=main --push-option=merge_request.merge_when_pipeline_succeeds --push-option=merge_request.remove_source_branch
```

* Set `$CREATE_MERGE_REQUEST` as a CI variable, then run the pipeline, to create a commit on a branch.

## Further Reading

* [Cron Job Syntax](crontab.guru)
* [Gitlab Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
* [Gitlab CI `!reference` tags](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags)
* [Gitlab CI job control](https://docs.gitlab.com/ee/ci/jobs/job_control.html#common-if-clauses-for-rules)

## TODOs

* ci-token-maintainer-api expires on 1 Jul (write-repository)
